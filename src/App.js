import { useDispatch, useSelector } from "react-redux";
import { fetchUsers } from "./redux/slice/user";
import "./App.css";
import {Router, Routes,Route, Link} from 'react-router-dom'
import ShowUsers from "./ShowUsers";

function App() {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  console.log("State", state);

  // if (state.user.isLoading) {
  //   return <h1>Loading....</h1>;
  // }

  return (
    <>
      <div className="App">
        <button onClick={(e) => dispatch(fetchUsers())}>Fetch Users</button>
        {state.user.data && state.user.data.map((e) => <li>{e.name}</li>)}
      </div>
      <Router>
      <div>
        <Link className="lnk" to="/">
          Home
        </Link>
        {"  "}||{"  "}
        <Link className="lnk" to="/ShowUsers">
          Users
        </Link>
      </div>
      <div>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/ShowUsers" element={<ShowUsers />} />
        </Routes>
      </div>
    </Router>
    </>

  );
}

export default App;
